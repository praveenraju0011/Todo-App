const todo = require("../models/Todo");

const router = require("express").Router();

//Home route

router.get("/",async(req,res)=>{
    
    const disTodo = await todo.find();
    
    
    res.render('index',{todo: disTodo});
});




module.exports = router;