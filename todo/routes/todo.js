const router = require("express").Router();

const Todo = require("../models/Todo")

//todo route


//create api
router.post('/add/todo', (req,res) =>{
    const todo = req.body.todo;
    console.log(todo);

    //new todo creation
    const newTodo = new Todo({todo,})
    

    //Saving todo to db
    newTodo
    .save()
    .then(()=>{
        console.log('sucessfully added');
        res.redirect('/');
    })
    .catch(err => console.log(err))
})


//delete api
router.get('/delete/one/:_id', (req,res)=>{
    const id = req.params;

    Todo.deleteOne({id})
    .then(() =>{
        console.log("Deleted");
        res.redirect("/");
    })
    .catch((err) => console.log(err));
});


//delete all
router.get('/delete/all', (req,res)=>{
    

    Todo.collection.remove({})
    .then(()=>{
        console.log("Deleted all");
        res.redirect("/");
    })
    .catch((err) => console.log(err));
    
});


module.exports = router;